+++
template = 'home.html'

[extra]
lang = 'en'
+++

Reproducible Infrastructure and Packaging Nix & NixOS.

If you enjoy my work, you might be interested in hiring [me](/contact).
